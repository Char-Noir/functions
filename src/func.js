const getSum = (str1, str2) => {
  //check if str1 and str2 are strings
  if (typeof str1 !== "string" || typeof str2 !== "string") {
    return false;
  }
  //if str empty set it to 0  
  if (str1 === "") {
    str1 = '0';
  }
  if (str2 === "") {
    str2 = '0';
  }
  //check if str1 or str2 only numbers characters
  if (!(/^\d+$/.test(str1)) || !(/^\d+$/.test(str2))) {
    return false;
  }
  

  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for (const post of listOfPosts) {
    if (post.author === authorName) {
      posts++;
    }
    if(post.comments){
      for (const comment of post.comments) {
        if (comment.author === authorName) {
          comments++;
        }
      }
    }
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  let cashbox = {};
  people = people.map(el => {
    return Number(el);
  });
  for (const element of people) {
    if(element==25){
      cashbox[element] = cashbox[element] ? cashbox[element] + 1 : 1;
    }
    else{
      if(canWeReturnExcange(cashbox,element-25)){
        cashbox[element] = cashbox[element] ? cashbox[element] + 1 : 1;
      }
      else{
        return 'NO';
      }
    }
    const ordered = sortObject(cashbox);
    cashbox = ordered;
  }
  return 'YES';
};

function canWeReturnExcange(cashbox, el){
    for (const key in cashbox) {
      if(cashbox[key]==0){
        continue;
      }
      if(el>=key){
        cashbox[key]--;
        el-=key;
      }
    }
    return el<=0;
}

const sortObject = obj => Object.keys(obj).sort().reduce((res, key) => { 
  res[key] = obj[key]; 
  return res;
}, {})

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
